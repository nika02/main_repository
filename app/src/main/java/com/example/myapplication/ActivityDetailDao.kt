package com.example.myapplication

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface ActivityDetailDao {

    @Query("SELECT * FROM ActivityDetails")
    fun getAllActivityDetail() : List<ActivityDetail>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg ActivityDetails: ActivityDetail)

    @Query("DELETE FROM ActivityDetails")
    fun deleteAll()
}