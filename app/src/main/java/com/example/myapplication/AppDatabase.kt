package com.example.myapplication

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [ActivityDetail::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getActivityDetailDao() : ActivityDetailDao
}