package com.example.myapplication

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "ActivityDetails")
data class ActivityDetail(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ActivityDetail_ID")
    val id: Int,

    @ColumnInfo(name = "MileageDistance")
    val MileageDistance: Double,

    @ColumnInfo(name = "SwimmingDistance")
    val SwimmingDistance: Double,

    @ColumnInfo(name = "CaloriesTaken")
    val CaloriesTaken: Double
)
