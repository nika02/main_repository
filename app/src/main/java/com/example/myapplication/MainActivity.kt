package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.mbms.MbmsErrors
import android.util.Log
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    private lateinit var resultMileageDistance: TextView
    private lateinit var resultMileageDistanceText: TextView
    private lateinit var resultSwimmingDistance: TextView
    private lateinit var resultSwimmingDistanceText: TextView
    private lateinit var resultCaloriesTaken: TextView
    private lateinit var resultCaloriesTakenText: TextView
    private lateinit var resultSumMileageDistanceText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultMileageDistance = findViewById(R.id.MileageDistance)
        resultMileageDistanceText = findViewById(R.id.MileageDistanceText)
        resultSwimmingDistance = findViewById(R.id.SwimmingDistance)
        resultSwimmingDistanceText = findViewById(R.id.SwimmingDistanceText)
        resultCaloriesTaken = findViewById(R.id.CaloriesTaken)
        resultCaloriesTakenText = findViewById(R.id.CaloriesTakenText)
        resultSumMileageDistanceText = findViewById(R.id.SumMileageDistanceText)

        App.instance.db.getActivityDetailDao().deleteAll()
    }

    @SuppressLint("SetTextI18n")
    fun buttonClick(view: View) {
        if(view is Button) {
            insertActivityDetail()

            val result = getAllActivityDetails()

            setAllValues(result)

            clearInputs()
        }
    }

    private fun clearInputs() {
        resultMileageDistance.text = ""
        resultSwimmingDistance.text = ""
        resultCaloriesTaken.text = ""
    }

    private fun setAllValues(result: List<ActivityDetail>) {
        val mileageDistances: MutableList<Double> = mutableListOf<Double>()
        val swimmingDistances: MutableList<Double> = mutableListOf<Double>()
        val caloriesTaken: MutableList<Double> = mutableListOf<Double>()

        result.forEach{
            activityDetail ->
            run {
                mileageDistances.add(activityDetail.MileageDistance)
                swimmingDistances.add(activityDetail.SwimmingDistance)
                caloriesTaken.add(activityDetail.CaloriesTaken)
            }
        }

        resultMileageDistanceText.text = mileageDistances.average().toString()
        resultSwimmingDistanceText.text = swimmingDistances.average().toString()
        resultCaloriesTakenText.text = caloriesTaken.average().toString()
        resultSumMileageDistanceText.text = mileageDistances.sum().toString()
    }

    private fun insertActivityDetail() {
        val resultMileageDistance = resultMileageDistance.text.toString().toDouble()
        val resultSwimmingDistance = resultSwimmingDistance.text.toString().toDouble()
        val resultCaloriesTaken = resultCaloriesTaken.text.toString().toDouble()

        App.instance.db.getActivityDetailDao().insert(ActivityDetail(0, resultMileageDistance, resultSwimmingDistance, resultCaloriesTaken))
    }

    private fun getAllActivityDetails(): List<ActivityDetail> {
        return App.instance.db.getActivityDetailDao().getAllActivityDetail()
    }
}