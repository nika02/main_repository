package com.example.myapplication

import android.app.Application
import android.content.Context
import android.provider.DocumentsContract
import androidx.room.Room

class App : Application() {

    lateinit var db: AppDatabase

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        db = Room
            .databaseBuilder(
                applicationContext,
                AppDatabase::class.java,
                "App_DB_Name_1"
            )
            .allowMainThreadQueries()
            .build()
    }

    override fun getApplicationContext(): Context {
        return super.getApplicationContext()
    }


}